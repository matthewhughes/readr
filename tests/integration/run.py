import sqlalchemy
import unittest

from readr import db, config, entities


def main():
    test_config = config.Testing()
    db_uri = test_config.DB_URI
    db.init_db(db_uri)

    testsuite = unittest.TestLoader().discover(".")
    unittest.TextTestRunner(verbosity=1).run(testsuite)

    _teardown_test_db(db_uri)


def _teardown_test_db(db_uri):
    engine = sqlalchemy.create_engine(db_uri)
    entities.Base.metadata.drop_all(engine)


if __name__ == "__main__":
    main()
