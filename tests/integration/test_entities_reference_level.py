from sqlalchemy import exc

from readr.entities import language, reference_level
from . import utils


class TestReferenceLevelCRUD(utils.DBTestCase):
    def test_reference_level_creation(self):
        level = reference_level.ReferenceLevel(label="B1", name="Intermediate")
        self.cleanup.append(level)

        self.session.add(level)
        self.session.commit()

        query_result = (
            self.session.query(reference_level.ReferenceLevel)
            .filter_by(id=level.id)
            .first()
        )
        self.assertEqual(query_result, level)

    def test_reference_level_update(self):
        level = reference_level.ReferenceLevel(label="A1", name="Elementary")
        self.cleanup.append(level)

        self.session.add(level)
        self.session.commit()

        label = "A2"
        level.label = label
        self.session.commit()

        query_result = (
            self.session.query(reference_level.ReferenceLevel)
            .filter_by(id=level.id)
            .first()
        )
        self.assertEqual(query_result, level)

    def test_creation_with_invalid_label(self):
        label = "D2"
        level = reference_level.ReferenceLevel(label=label, name="invalid")

        self.session.add(level)
        with self.assertRaises(exc.DataError):
            self.session.commit()
        self.session.rollback()


class TestReferenceLevelTranslationCRUD(utils.DBTestCase):
    def test_reference_level_translation_create(self):
        level = reference_level.ReferenceLevel(label="C2", name="Proficient")
        lang = language.Language(name="German", code="de-DE")
        self.cleanup.extend((level, lang))

        self.session.add_all([level, lang])
        self.session.commit()

        reference_translation = reference_level.ReferenceLevelTranslation(
            name_translation="Proficiency",
            reference_level_id=level.id,
            language_id=lang.id,
        )
        self.cleanup.append(reference_translation)

        self.session.add(reference_translation)
        self.session.commit()
        query_result = self.session.query(
            reference_level.ReferenceLevelTranslation
        ).filter_by(id=reference_translation.id)
        self.assertEqual(query_result[0], reference_translation)

        # Check relationships
        self.assertEqual(reference_translation.reference_level, level)
        self.assertEqual(reference_translation.language, lang)
        self.assertEqual(level.translations, [reference_translation])
