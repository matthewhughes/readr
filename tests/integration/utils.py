import contextlib
import flask
import sqlalchemy
import unittest

from sqlalchemy import orm

from readr import config
from readr.entities import book, language, reference_level

_CONFIG = config.Testing()


class DBTestCase(unittest.TestCase):
    engine = sqlalchemy.create_engine(
        "postgresql+psycopg2://{}:{}@{}/{}".format(
            # maybe move to _CONFIG.DB_URI
            _CONFIG.DB_USER,
            _CONFIG.DB_PASSWORD,
            _CONFIG.DB_HOST,
            _CONFIG.DB_NAME,
        )
    )
    Session = orm.sessionmaker(bind=engine)

    def setUp(self):
        self.session = self.Session()
        self.cleanup = []

    def tearDown(self):
        if self.cleanup:
            for obj in reversed(self.cleanup):
                self.session.delete(obj)
            self.session.commit()
        self.session.close()


def build_test_language():
    return language.Language(name="English", code="en-UK")


def build_test_reference_level():
    return reference_level.ReferenceLevel(label="B2", name="Upper Intermediate")


def build_test_book(lang, level):
    return book.Book(
        isbn10="0316769177",
        isbn13="9780316769174",
        title="The Catcher in the Rye",
        language_id=lang.id,
        reference_level_id=level.id,
    )


# Taken from: https://flask.palletsprojects.com/en/1.1.x/signals/
@contextlib.contextmanager
def captured_templates(app):
    recorded = []

    def record(sender, template, context, **extra):
        recorded.append((template, context))

    flask.template_rendered.connect(record, app)
    try:
        yield recorded
    finally:
        flask.template_rendered.disconnect(record, app)
