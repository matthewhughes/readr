from http import HTTPStatus

from readr import app
from . import utils


class TestBooksRoute(utils.DBTestCase):
    endpoint = "books/"
    foo_app = app.create_app()
    client = foo_app.test_client()

    def test_get_books(self):
        lang = utils.build_test_language()
        ref_level = utils.build_test_reference_level()
        self.cleanup.extend((lang, ref_level))
        self.session.add_all((lang, ref_level))
        self.session.commit()

        book = utils.build_test_book(lang, ref_level)
        self.cleanup.append(book)
        self.session.add(book)
        self.session.commit()

        with utils.captured_templates(self.foo_app) as templates:
            resp = self.client.get(self.endpoint)

        self.assertEqual(len(templates), 1)
        template, context = templates[0]
        self.assertEqual(template.name, "books/index.html")

        self.assertEqual(len(context["books"]), 1)
        rendered_book = context["books"][0]
        self.assertEqual(rendered_book.id, book.id)

        self.assertEqual(resp.status_code, HTTPStatus.OK)
