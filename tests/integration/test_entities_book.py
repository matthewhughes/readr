from sqlalchemy import exc

from readr.entities import book
from . import utils


class TestBookCreation(utils.DBTestCase):
    def setUp(self):
        super().setUp()
        self.lang = utils.build_test_language()
        self.ref_level = utils.build_test_reference_level()
        self.session.add_all((self.lang, self.ref_level))
        self.cleanup.extend((self.lang, self.ref_level))
        self.session.commit()

    def test_book_creation(self):
        new_book = book.Book(
            isbn10="0316769177",
            isbn13="9780316769174",
            title="The Catcher in the Rye",
            language_id=self.lang.id,
            reference_level_id=self.ref_level.id,
        )
        self.cleanup.append(new_book)
        self.session.add(new_book)
        self.session.commit()

        query_result = self.session.query(book.Book).filter_by(id=new_book.id).first()
        self.assertEqual(query_result, new_book)

        self.assertEqual(new_book.language, self.lang)
        self.assertEqual(new_book.reference_level, self.ref_level)

    def test_book_creation_with_invalid_isbn10(self):
        new_book = book.Book(
            isbn10="031",
            isbn13="9780316769174",
            title="The Catcher in the Rye",
            language_id=self.lang.id,
            reference_level_id=self.ref_level.id,
        )
        self.session.add(new_book)
        with self.assertRaises(exc.IntegrityError):
            self.session.commit()
        self.session.rollback()

    def test_book_creation_with_invalid_isbn13(self):
        new_book = book.Book(
            isbn10="0316769177",
            isbn13="9780",
            title="The Catcher in the Rye",
            language_id=self.lang.id,
            reference_level_id=self.ref_level.id,
        )
        self.session.add(new_book)
        with self.assertRaises(exc.IntegrityError):
            self.session.commit()
        self.session.rollback()


class TestBookTranslationCreation(utils.DBTestCase):
    def setUp(self):
        super().setUp()
        self.lang = utils.build_test_language()
        self.ref_level = utils.build_test_reference_level()
        self.cleanup.extend((self.lang, self.ref_level))
        self.session.add_all((self.lang, self.ref_level))
        self.session.commit()

        self.book = book.Book(
            isbn10="0316769177",
            isbn13="9780316769174",
            title="The Catcher in the Rye",
            language_id=self.lang.id,
            reference_level_id=self.ref_level.id,
        )
        self.cleanup.append(self.book)
        self.session.add(self.book)
        self.session.commit()

    def test_book_translation_creation(self):
        translation = book.BookTranslation(
            book_id=self.book.id,
            title_translation="L'Attrape-cœurs",
            language_id=self.lang.id,
        )
        self.cleanup.append(translation)
        self.session.add(translation)
        self.session.commit()

        query_result = (
            self.session.query(book.BookTranslation)
            .filter_by(id=translation.id)
            .first()
        )

        self.assertEqual(query_result, translation)

        self.assertEqual(translation.language, self.lang)
        self.assertEqual(translation.book, self.book)
        self.assertEqual(self.book.translations, [translation])
