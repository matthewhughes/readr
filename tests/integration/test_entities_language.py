from readr.entities import language

from . import utils


class TestLanguageCRUD(utils.DBTestCase):
    def test_language_creation(self):
        lang_name = "English"
        lang = language.Language(name=lang_name, code="en-UK")
        self.cleanup.append(lang)
        self.session.add(lang)
        self.session.commit()

        query_result = self.session.query(language.Language).first()
        self.assertEqual(query_result, lang)

    def test_language_update(self):
        lang = language.Language(name="French", code="fr-FR")
        self.cleanup.append(lang)
        self.session.add(lang)
        self.session.commit()

        code = "fr-BE"
        lang.code = code
        self.session.commit()

        query_result = (
            self.session.query(language.Language).filter_by(id=lang.id).first()
        )
        self.assertEqual(query_result.code, code)


class TestLanguageTranslationCRUD(utils.DBTestCase):
    def setUp(self):
        super().setUp()
        self.lang = language.Language(name="English", code="en-US")
        self.cleanup.append(self.lang)
        self.session.add(self.lang)
        self.session.commit()

    def test_language_translation_creation(self):
        translation = language.LanguageTranslation(
            name_translation="Anglais", language_id=self.lang.id
        )
        self.cleanup.append(translation)
        self.session.add(translation)
        self.session.commit()

        query_result = (
            self.session.query(language.LanguageTranslation)
            .filter_by(id=translation.id)
            .all()
        )
        self.assertEqual(len(query_result), 1)
        our_translation = query_result[0]
        self.assertEqual(translation, our_translation)

        # Verify relationships are built
        self.assertEqual(translation.language, self.lang)
        self.assertEqual(self.lang.translations, [translation])
