NAME=readr
LIB_DIR=readr
TEST_DIR=tests

.PHONY: help build install dist unit_test test_coverage test_end_to_end test \
	lint format_code check_code_format verify

test_integration:
	PYTHONPATH=$(shell pwd) FLASK_ENV=testing python3 tests/integration/run.py

test: test_integration

lint:
	flake8 $(LIB_DIR) $(TEST_DIR)

format_code:
	black $(LIB_DIR) $(TEST_DIR)

check_code_format:
	black --check $(LIB_DIR) $(TEST_DIR)

verify: test lint check_code_format
