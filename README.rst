=====
Readr
=====

A proof of concept/learning opportunity for a book listing website using
``Flask``, ``SQLAlchemy``, and ``PostgreSQL``. There isn't really any content,
but if you want to run it:

::

   $ pip install --requirement requirements.txt
   $ FLASK_ENV=development FLASK_APP=readr.app flask run

There are also tests available:

::

   $ pip install --requirement requirements_dev.txt
   $ make test

