"""Centralised db access, for use with or without a Flask app"""
import sqlalchemy

from sqlalchemy import orm

from readr import entities

# Import models to populate metadata
from readr.entities import book, language, reference_level  # noqa F401


def init_db(db_uri):
    engine = sqlalchemy.create_engine(db_uri)
    # TODO: load all entities before this
    entities.Base.metadata.create_all(engine)


def connect(db_uri):
    engine = sqlalchemy.create_engine(db_uri)
    Session = orm.sessionmaker(bind=engine)
    return Session()


def close_db(session):
    session.close()
