import sqlalchemy

from sqlalchemy import orm
from sqlalchemy.dialects import postgresql

from readr import entities

# https://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages
_cefrl_level = postgresql.ENUM("A1", "A2", "B1", "B2", "C1", "C2", name="cefrl_level")


class ReferenceLevelTranslation(entities.Base):
    __tablename__ = "reference_level_translation"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name_translation = sqlalchemy.Column(sqlalchemy.String(100), nullable=False)
    reference_level_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("reference_level.id"), nullable=False,
    )
    language_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("language.id"), nullable=False
    )

    reference_level = orm.relationship("ReferenceLevel", back_populates="translations")
    language = orm.relationship("Language")


class ReferenceLevel(entities.Base):
    __tablename__ = "reference_level"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(100), nullable=False)
    label = sqlalchemy.Column(_cefrl_level, unique=True)

    translations = orm.relationship(
        "ReferenceLevelTranslation",
        order_by=ReferenceLevelTranslation.id,
        back_populates="reference_level",
    )
