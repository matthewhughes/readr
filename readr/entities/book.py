import sqlalchemy

from sqlalchemy import orm

from readr import entities


class BookTranslation(entities.Base):
    __tablename__ = "book_translation"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    title_translation = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    book_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("book.id"), nullable=False
    )
    language_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("language.id"), nullable=False
    )

    book = orm.relationship("Book", back_populates="translations")
    language = orm.relationship("Language")


class Book(entities.Base):
    __tablename__ = "book"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    isbn10 = sqlalchemy.Column(
        sqlalchemy.String(10),
        sqlalchemy.CheckConstraint("isbn10 ~ '^[[:digit:]]{10}$'"),
        nullable=False,
    )
    isbn13 = sqlalchemy.Column(
        sqlalchemy.String(13),
        sqlalchemy.CheckConstraint("isbn13 ~ '^[[:digit:]]{13}$'"),
        nullable=False,
    )
    title = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    language_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("language.id"), nullable=False
    )
    reference_level_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("reference_level.id"), nullable=False,
    )

    language = orm.relationship("Language")
    reference_level = orm.relationship("ReferenceLevel")
    translations = orm.relationship(
        "BookTranslation", order_by=BookTranslation.id, back_populates="book"
    )
