import sqlalchemy

from sqlalchemy import orm

from readr import entities


class LanguageTranslation(entities.Base):
    __tablename__ = "language_translation"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name_translation = sqlalchemy.Column(sqlalchemy.String(100), nullable=False)
    language_id = sqlalchemy.Column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey("language.id"), nullable=False
    )

    language = orm.relationship("Language", back_populates="translations")


class Language(entities.Base):
    __tablename__ = "language"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(100), nullable=False)
    code = sqlalchemy.Column(sqlalchemy.String(10), nullable=False, unique=True)

    translations = orm.relationship(
        "LanguageTranslation",
        order_by=LanguageTranslation.id,
        back_populates="language",
    )
