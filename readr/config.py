class Base:
    @property
    def DB_URI(self):
        return "postgresql+psycopg2://{user}:{password}@{host}/{name}".format(
            user=self.DB_USER,
            password=self.DB_PASSWORD,
            host=self.DB_HOST,
            name=self.DB_NAME,
        )


class Testing(Base):
    DB_HOST = "localhost"
    DB_PASSWORD = "readr_test"
    DB_USER = "readr_test"
    DB_NAME = "readr_test"


class Development(Base):
    DB_HOST = "localhost"
    DB_PASSWORD = "readr"
    DB_USER = "readr"
    DB_NAME = "readr"
    DB_URI = "postgresql+psycopg2://{user}:{password}@{host}/{name}".format(
        user=DB_USER, password=DB_PASSWORD, host=DB_HOST, name=DB_NAME,
    )
