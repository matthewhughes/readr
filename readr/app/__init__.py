import flask

from werkzeug import utils

from readr.app import db
from readr.app.routes import main, books


def create_app(app_env=None):
    app = flask.Flask(__name__, template_folder="templates")

    load_config(app)
    db.init_app(app)
    register_routes(app)
    return app


def load_config(app):
    config_class = "readr.app.config." + app.env.capitalize()
    config = utils.import_string(config_class)()
    app.config.from_object(config)


def register_routes(app):
    app.register_blueprint(main.bp)
    app.register_blueprint(books.bp)
