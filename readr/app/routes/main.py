import flask

bp = flask.Blueprint("main", __name__, url_prefix="/")


@bp.route("/", methods=["GET"])
def greet():
    return flask.render_template("index.html")
