import flask

from readr.app import db
from readr.entities import book

bp = flask.Blueprint("books", __name__, url_prefix="/books")


@bp.route("/", methods=["GET"])
def list_books():
    books = _get_books(db.get_db())
    return flask.render_template("books/index.html", books=books)


def _get_books(db):
    books = db.query(book.Book).all()
    return books
