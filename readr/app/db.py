import click
import flask

import readr.db


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


def get_db():
    config = flask.current_app.config
    if "db" not in flask.g:
        flask.g.db = readr.db.connect(config["DB_URI"])
    return flask.g.db


def close_db(e=None):
    db = flask.g.pop("db", None)

    if db is not None:
        readr.db.close_db(db)


@click.command("init-db")
@flask.cli.with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    db = get_db()
    readr.db.init_db(db)
    click.echo("Initialized the database.")
