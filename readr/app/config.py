from readr import config


class Base:
    DEBUG = False
    TESTING = False


class Testing(config.Testing, Base):
    DEBUG = True
    TESTING = True


class Development(config.Development, Base):
    # Flask specific config
    pass
